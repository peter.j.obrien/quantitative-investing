# Quantitative Investing

Notebooks for simulating leveraged ETFs, robust Monte Carlo backtesting, and dynamic allocation.


### Leveraged ETFs [[View](https://nbviewer.jupyter.org/urls/gitlab.com/doctorj/quantitative-investing/-/raw/master/Leveraged%20ETFs.ipynb), [Run](https://mybinder.org/v2/gl/doctorj%2Fquantitative-investing/master?filepath=Leveraged%20ETFs.ipynb)]: Extend leveraged ETFs back in time

You can also just [download the data as CSV](extended-leveraged-etfs.csv) for a few extended ETFs.
